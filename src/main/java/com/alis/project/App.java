package com.alis.project;

import android.annotation.SuppressLint;
import android.app.Application;
import com.parse.Parse;
import com.parse.ParseObject;

@SuppressLint("Registered")
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        try {
            Parse.Configuration.Builder cb = new Parse.Configuration.Builder(this);
            cb.applicationId("zyjbpFVCKeZRsaKzIRjLWBEFsCLpRTfh0uzbAbt1");
            cb.clientKey("kX2GMdJrpHbOfqgFCyUYzKWdHafOCGzYw454gSdR");
            cb.server("https://parseapi.back4app.com/");
            cb.enableLocalDataStore();
            ParseObject.registerSubclass(Location.class);
            Parse.Configuration c = cb.build();
            Parse.initialize(c);
        }catch (Exception ignored){}
    }

}
