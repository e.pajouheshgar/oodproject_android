package com.alis.project;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

@ParseClassName("Location")
public class Location extends ParseObject {

    public void setLat(double lat) {
        put("lat", lat);
    }

    public void setLong(double _long) {
        put("l_long", _long);
    }
    public void setUser(String usrId) {
        put("usrId", usrId);
    }

    public double getLat() {
        return getDouble("lat");
    }

    public double getLong() {
        return getDouble("_long");
    }
}
