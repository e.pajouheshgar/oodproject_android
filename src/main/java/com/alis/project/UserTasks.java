package com.alis.project;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

public class UserTasks extends AppCompatActivity {

    ListView rc_tasks;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_tasks);
        rc_tasks = findViewById(R.id.rc_tasks);
        new ParseQuery<ParseObject>("Shipment")
                .whereEqualTo("Driver", ParseUser.getCurrentUser().getObjectId()).findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> list, ParseException e) {
                try {
                    ArrayList<String> tasks = new ArrayList<String>();
                    for (ParseObject pp :
                            list) {
                        tasks.add(pp.getString("BuyerLastName")+" - "+pp.getString("Description"));
                    }
                    if (list.size() == 0) {
                        tasks.add("No Task");
                    }
                    rc_tasks.setAdapter(new ArrayAdapter<String>(UserTasks.this, R.layout.task_item, R.id.tvName, tasks));
                } catch (Exception ignored) {

                }
            }
        });
    }
}
